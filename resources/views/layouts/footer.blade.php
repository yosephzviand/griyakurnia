<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
{{-- <footer class="main-footer">
            <strong>Copyright &copy; {{ date('Y') }} <a href="https://adminlte.io">STARIA</a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 3.2.0
            </div>
        </footer> --}}
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('template') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ asset('template') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('template') }}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('template') }}/dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('template') }}/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="{{ asset('template') }}/plugins/raphael/raphael.min.js"></script>
<script src="{{ asset('template') }}/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="{{ asset('template') }}/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="{{ asset('template') }}/plugins/chart.js/Chart.min.js"></script>
<script src="{{ asset('template') }}/plugins/toastr/toastr.min.js"></script>

<!-- AdminLTE for demo purposes -->
{{-- <script src="{{ asset('template') }}/dist/js/demo.js"></script> --}}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{ asset('template') }}/dist/js/pages/dashboard2.js"></script> --}}


@if (!empty(Session::get('status')) && Session::get('status') == '1')
    <script>
        $message = "{{ Session::get('message') }}";
        toastr.remove();
        toastr.success($message);
    </script>
@endif

@if (!empty(Session::get('status')) && Session::get('status') == '2')
    <script>
        $message = "{{ Session::get('message') }}";
        toastr.remove();
        toastr.warning($message)
    </script>
@endif

<script>
    $('#table').DataTable({});
</script>

<script>
    $(document).on('click', '.editwarga', function() {
        var id = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ route('warga.edit') }}",
            method: "POST",
            data: {
                id: id
            },
            success: function(data) {
                $('[name="idedit"]').val(data.id);
                $('[name="blok"]').val(data.blok);
                $('[name="norumah"]').val(data.norumah);
                $('[name="nama"]').val(data.nama);
            }
        });
    });

    $(document).on('click', '.edituser', function() {
        var id = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ route('user.edit') }}",
            method: "POST",
            data: {
                id: id
            },
            success: function(data) {
                $('[name="idedit"]').val(data.id);
                $('[name="nama"]').val(data.name);
                $('[name="email"]').val(data.email);
            }
        });
    });

    $(document).on('click', '.editkeluar', function() {
        var id = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ route('keluar.edit') }}",
            method: "POST",
            data: {
                id: id
            },
            success: function(data) {
                $('[name="idedit"]').val(data.id);
                $('[name="bulan"]').val(data.bulan);
                $('[name="deskripsi"]').val(data.deskripsi);
                $('[name="nominal"]').val(data.nominal);
            }
        });
    });

    $(document).on('click', '.editsetting', function() {
        var id = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ route('settingkas.edit') }}",
            method: "POST",
            data: {
                id: id
            },
            success: function(data) {
                $('[name="idedit"]').val(data.id);
                $('[name="nominal"]').val(data.nominal);
            }
        });
    });

    $('#bulan').change(function() {
        var bulan = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('kas.bulan') }}",
            method: "POST",
            data: {
                bulan: bulan
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var html = '';
                var i;
                html = '<option value="" disabled selected hidden>Pilih </option>';
                for (i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i].id + '>' + data[i].nama +
                        '</option>';
                }
                $('#warga').html(html);
            }
        });
        return false;
    });
</script>
</body>

</html>
