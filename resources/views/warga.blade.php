@extends('layouts.appadmin')
<!-- Main Sidebar Container -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Warga</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Warga</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-primary">
                            <form method="post" action="{{ route('warga.store') }}">
                                @csrf
                                <input type="hidden" name="idedit" value="">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="">Blok</label>
                                        <select name="blok" class="form-control" id="blok" required>
                                            <option value="" disabled selected hidden>Pilih </option>
                                            @foreach ($blok as $b)
                                                <option value="{{ $b->id }}">{{ $b->nama }}</option>
                                            @endforeach
                                        </select>
                                        @error('blok')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">No Rumah</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name="norumah"
                                            placeholder="Tuliskan" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nama Warga</label>
                                        <input type="text" name="nama" class="form-control" id="nama"
                                            placeholder="Tuliskan " value="" autocomplete="off" required>
                                        @error('nama')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <table id="table" class="table table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Alamat</th>
                                            <th>Nama</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $i => $d)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $d->toblok->nama }} {{ $d->norumah }}</td>
                                                <td>{{ $d->nama }}</td>
                                                <td>
                                                    <button type="submit" class="btn btn-primary btn-sm editwarga"
                                                        data-id="{{ $d->id }}">Edit</button>
                                                    <a href="{{ route('warga.delete', $d->id) }}"
                                                        class="btn btn-danger btn-sm">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
