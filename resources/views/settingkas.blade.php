@extends('layouts.appadmin')
<!-- Main Sidebar Container -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Setting KAS</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Setting KAS</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-primary">
                            <form method="post" action="{{ route('settingkas.store') }}">
                                @csrf
                                <input type="hidden" name="idedit" value="">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="">Nominal KAS</label>
                                        <input type="number" name="nominalkas" class="form-control" id="nominalkas"
                                            placeholder="Tuliskan " value="" autocomplete="off" required>
                                        @error('nominalkas')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nominal Sosial</label>
                                        <input type="number" name="nominalsosial" class="form-control" id="nominalsosial"
                                            placeholder="Tuliskan " value="" autocomplete="off" required>
                                        @error('nominalsosial')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <table id="table" class="table table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nominal KAS</th>
                                            <th>Nominal Sosial</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $i => $d)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ number_format($d->nominalkas, 0) }}</td>
                                                <td>{{ number_format($d->nominalsosial, 0) }}</td>
                                                <td>
                                                    <button type="submit" class="btn btn-primary btn-sm editsetting"
                                                        data-id="{{ $d->id }}">Edit</button>
                                                    <a href="{{ route('settingkas.delete', $d->id) }}"
                                                        class="btn btn-danger btn-sm">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
