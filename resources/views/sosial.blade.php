@extends('layouts.appadmin')
<!-- Main Sidebar Container -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Sosial</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Sosial</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    {{-- <div class="col-md-4">
                        <div class="card card-primary">
                            <form method="post" action="{{ route('kas.store') }}">
                                @csrf
                                <input type="hidden" name="idedit" value="">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="">Bulan</label>
                                        <select name="bulan" class="form-control" id="bulan" required>
                                            <option value="" disabled selected hidden>Pilih </option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                        @error('blok')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nama Warga</label>
                                        <select name="warga" class="form-control" id="warga" required>
                                            <option value="" disabled selected hidden>Pilih </option>
                                            @foreach ($warga as $w)
                                                <option value="{{ $w->id }}">{{ $w->nama }}</option>
                                            @endforeach
                                        </select>
                                        @error('warga')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="table" class="table table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Bulan</th>
                                            <th>Nominal</th>
                                            {{-- <th>Aksi</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $i => $d)
                                            @php
                                                $bulan = '';
                                                if ($d->bulan == 1) {
                                                    $bulan = 'Januari';
                                                } elseif ($d->bulan == 2) {
                                                    $bulan = 'Februari';
                                                } elseif ($d->bulan == 3) {
                                                    $bulan = 'Maret';
                                                } elseif ($d->bulan == 4) {
                                                    $bulan = 'April';
                                                } elseif ($d->bulan == 5) {
                                                    $bulan = 'Mei';
                                                } elseif ($d->bulan == 6) {
                                                    $bulan = 'Juni';
                                                } elseif ($d->bulan == 7) {
                                                    $bulan = 'Juli';
                                                } elseif ($d->bulan == 8) {
                                                    $bulan = 'Agustus';
                                                } elseif ($d->bulan == 9) {
                                                    $bulan = 'September';
                                                } elseif ($d->bulan == 10) {
                                                    $bulan = 'Oktober';
                                                } elseif ($d->bulan == 11) {
                                                    $bulan = 'November';
                                                } elseif ($d->bulan == 12) {
                                                    $bulan = 'Desember';
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $d->towarga->nama }}</td>
                                                <td>{{ $d->towarga->toblok->nama }} {{ $d->towarga->norumah }}</td>
                                                <td>{{ $bulan }}</td>
                                                <td>{{ number_format($d->nominal, 0) }}</td>
                                                {{-- <td>
                                                    <a href="{{ route('kas.delete', $d->id) }}"
                                                        class="btn btn-danger btn-sm">Hapus</a>
                                                </td> --}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
