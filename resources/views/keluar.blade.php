@extends('layouts.appadmin')
<!-- Main Sidebar Container -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Pengeluaran</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Pengeluaran</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-primary">
                            <form method="post" action="{{ route('keluar.store') }}">
                                @csrf
                                <input type="hidden" name="idedit" value="">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="">Bulan</label>
                                        <select name="bulan" class="form-control" id="bulan" required>
                                            <option value="" disabled selected hidden>Pilih </option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                        @error('blok')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jenis Pengeluaran</label>
                                        <select name="jenis" class="form-control" id="jenis" required>
                                            <option value="" disabled selected hidden>Pilih </option>
                                            <option value="kas">KAS</option>
                                            <option value="sosial">Sosial</option>
                                        </select>
                                        @error("jenis") <div class="small text-danger">{{ message }}</div> @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="">Deskripsi</label>
                                        <textarea name="deskripsi" class="form-control" id="deskripsi" rows="5" required></textarea>
                                        @error('deskripsi')
                                            <div class="small text-danger">{{ message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nominal</label>
                                        <input type="number" name="nominal" class="form-control" id="nominal"
                                            placeholder="Tuliskan " value="" autocomplete="off" required>
                                        @error('nominal')
                                            <div class="small text-danger">{{ message }}</div>
                                        @enderror
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <table id="table" class="table table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Jenis</th>
                                            <th>Nominal</th>
                                            <th>Deskripsi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $i => $d)
                                            @php
                                                $bulan = '';
                                                if ($d->bulan == 1) {
                                                    $bulan = 'Januari';
                                                } elseif ($d->bulan == 2) {
                                                    $bulan = 'Februari';
                                                } elseif ($d->bulan == 3) {
                                                    $bulan = 'Maret';
                                                } elseif ($d->bulan == 4) {
                                                    $bulan = 'April';
                                                } elseif ($d->bulan == 5) {
                                                    $bulan = 'Mei';
                                                } elseif ($d->bulan == 6) {
                                                    $bulan = 'Juni';
                                                } elseif ($d->bulan == 7) {
                                                    $bulan = 'Juli';
                                                } elseif ($d->bulan == 8) {
                                                    $bulan = 'Agustus';
                                                } elseif ($d->bulan == 9) {
                                                    $bulan = 'September';
                                                } elseif ($d->bulan == 10) {
                                                    $bulan = 'Oktober';
                                                } elseif ($d->bulan == 11) {
                                                    $bulan = 'November';
                                                } elseif ($d->bulan == 12) {
                                                    $bulan = 'Desember';
                                                }

                                                if ($d->jenis == 1) {
                                                    $jenis = 'KAS';
                                                } else {
                                                    $jenis = 'SOSIAL';
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $bulan }}</td>
                                                <td>{{ $jenis }}</td>
                                                <td>{{ number_format($d->nominal, 0) }}</td>
                                                <td>{{ $d->deskripsi }}</td>
                                                <td>
                                                    <button type="submit" class="btn btn-primary btn-sm editkeluar"
                                                        data-id="{{ $d->id }}">Edit</button>
                                                    <a href="{{ route('keluar.delete', $d->id) }}"
                                                        class="btn btn-danger btn-sm">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
