<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/blok', [App\Http\Controllers\BlokController::class, 'index'])->name('blok');
Route::get('/deleteblok/{id}', [App\Http\Controllers\BlokController::class, 'delete'])->name('blok.delete');
Route::post('/blok', [App\Http\Controllers\BlokController::class, 'store'])->name('blok.store');

Route::get('/warga', [App\Http\Controllers\WargaController::class, 'index'])->name('warga');
Route::get('/deletewarga/{id}', [App\Http\Controllers\WargaController::class, 'delete'])->name('warga.delete');
Route::post('/warga', [App\Http\Controllers\WargaController::class, 'store'])->name('warga.store');
Route::post('/warga/edit', [App\Http\Controllers\WargaController::class, 'edit'])->name('warga.edit');

Route::get('/keuangan/masuk/kas', [App\Http\Controllers\KasController::class, 'index'])->name('kas');
Route::get('/keuangan/kasdelete/{id}', [App\Http\Controllers\KasController::class, 'delete'])->name('kas.delete');
Route::post('/keuangan/kas', [App\Http\Controllers\KasController::class, 'store'])->name('kas.store');
Route::post('/keuangan/bulankas', [App\Http\Controllers\KasController::class, 'bulankas'])->name('kas.bulan');

Route::get('/keuangan/masuk/sosial', [App\Http\Controllers\SosialController::class, 'index'])->name('sosial');
Route::get('/keuangan/sosialdelete/{id}', [App\Http\Controllers\SosialController::class, 'delete'])->name('sosial.delete');

Route::get('/keuangan/keluar', [App\Http\Controllers\KeluarController::class, 'index'])->name('keluar');
Route::get('/keuangan/keluar/delete/{id}', [App\Http\Controllers\KeluarController::class, 'delete'])->name('keluar.delete');
Route::post('/keuangan/keluar', [App\Http\Controllers\KeluarController::class, 'store'])->name('keluar.store');
Route::post('/keuangan/keluar/edit', [App\Http\Controllers\KeluarController::class, 'edit'])->name('keluar.edit');

Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->name('user');
Route::get('/userdelete/{id}', [App\Http\Controllers\UserController::class, 'delete'])->name('user.delete');
Route::post('/user', [App\Http\Controllers\UserController::class, 'store'])->name('user.store');
Route::post('/user/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');

Route::get('/settingkas', [App\Http\Controllers\SettingController::class, 'index'])->name('settingkas');
Route::get('/settingkas/delete/{id}', [App\Http\Controllers\SettingController::class, 'delete'])->name('settingkas.delete');
Route::post('/settingkas', [App\Http\Controllers\SettingController::class, 'store'])->name('settingkas.store');
Route::post('/settingkas/edit', [App\Http\Controllers\SettingController::class, 'edit'])->name('settingkas.edit');
