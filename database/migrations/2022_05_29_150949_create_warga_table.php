<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warga', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('blok')->unsigned();
            $table->bigInteger('norumah');
            $table->string('nama');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('warga', function ($table) {
            $table->foreign('blok')
                ->references('id')
                ->on('blok')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warga');
    }
}
