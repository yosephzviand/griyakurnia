<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSosialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sosial', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('warga')->unsigned();
            $table->integer('bulan');
            $table->bigInteger('nominal');
            $table->date('tanggal');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('sosial', function ($table) {
            $table->foreign('warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sosial');
    }
}
