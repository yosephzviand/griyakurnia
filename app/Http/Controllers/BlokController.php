<?php

namespace App\Http\Controllers;

use App\Models\Blok;
use Illuminate\Http\Request;

class BlokController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data   =   Blok::get();
        return view('blok', compact('data'));
    }

    public function store(Request $request)
    {
        $data   =   new Blok;

        $data->nama =   $request->nama;
        $data->save();

        return back()->with('status', 1)->with('message', 'Berhasil Simpan');
    }

    public function delete($id)
    {
        $data   =   Blok::find($id);
        $data->delete();

        return back()->with('status', 1)->with('message', 'Berhasil Hapus');
    }
}
