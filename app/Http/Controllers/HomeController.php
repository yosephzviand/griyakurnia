<?php

namespace App\Http\Controllers;

use App\Models\Kas;
use App\Models\Keluar;
use App\Models\Sosial;
use App\Models\Warga;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kas            =   Kas::sum('nominal');
        $sosial         =   Sosial::sum('nominal');
        $keluar         =   Keluar::sum('nominal');
        $keluarkas      =   Keluar::where('jenis', 1)->sum('nominal');
        $keluarsosial   =   Keluar::where('jenis', 2)->sum('nominal');
        $warga          =   Warga::count();
        $sisakas        =   $kas - $keluarkas;
        $sisasosial     =   $sosial - $keluarsosial;

        return view('homes', compact('kas', 'sosial', 'keluar', 'warga', 'sisakas', 'sisasosial', 'keluarkas', 'keluarsosial'));
    }
}
