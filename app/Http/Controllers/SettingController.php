<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data      =   Setting::get();
        return view('settingkas', compact('data'));
    }

    public function store(Request $request)
    {
        if ($request->idedit) {
            $data   =   Setting::find($request->idedit);
        } else {
            $data   =   new Setting();
        }

        $data->nominalkas       =   $request->nominalkas;
        $data->nominalsosial    =   $request->nominalsosial;
        $data->save();

        return back()->with('status', 1)->with('message', 'Berhasil Simpan');
    }

    public function edit(Request $request)
    {
        return Setting::find($request->id);
    }

    public function delete($id)
    {
        $data   =   Setting::find($id);
        $data->delete();

        return back()->with('status', 1)->with('message', 'Berhasil Hapus');
    }
}
