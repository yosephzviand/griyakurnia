<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data   =   User::get();
        return view('user', compact('data'));
    }

    public function store(Request $request)
    {
        if ($request->idedit == '') {
            $data  =   new User;
        } else {
            $data  =   User::find($request->idedit);
        }
        $data->name         =   $request->nama;
        $data->email        =   $request->email;
        $data->password     =   Hash::make($request->password);

        $data->save();

        return back()->with('status', 1)->with('message', 'Berhasil Simpan');
    }

    public function edit(Request $request)
    {
        return User::find($request->id);
    }

    public function delete($id)
    {
        $data   =   User::find($id);
        $data->delete();

        return back()->with('status', 1)->with('message', 'Berhasil Hapus');
    }
}
