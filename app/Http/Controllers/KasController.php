<?php

namespace App\Http\Controllers;

use App\Models\Kas;
use App\Models\Setting;
use App\Models\Sosial;
use App\Models\Warga;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $bulan  =   $request->bulan ?? date('m');
        $data   =   Kas::get();
        $warga  =   Warga::whereNotIn('id', Kas::select('warga')->where('bulan', $bulan))->get();
        return view('kas', compact('data', 'warga'));
    }

    public function bulankas(Request $request)
    {
        $bulan    =   $request->bulan ?? date('m');

        return Warga::whereNotIn('id', Kas::select('warga')->where('bulan', $bulan))->get();
    }

    public function store(Request $request)
    {
        if ($request->idedit) {
            $data   =   Kas::find($request->idedit);
        } else {
            $data   = new Kas();
            $sosial = new Sosial();
        }

        $nominal    =   Setting::first();
        if ($nominal) {
            $data->nominal  =   $nominal->nominalkas;
            $sosial->nominal  =   $nominal->nominalsosial;
        } else {
            return back()->with('status', 2)->with('message', 'Harap Isi Nominal KAS dan Sosial');
        }

        $data->warga    =   $request->warga;
        $data->bulan    =   $request->bulan;
        $data->tanggal  =   Carbon::now();
        $data->save();

        $sosial->warga    =   $request->warga;
        $sosial->bulan    =   $request->bulan;
        $sosial->tanggal  =   Carbon::now();
        $sosial->save();

        if ($nominal) {
            return back()->with('status', 1)->with('message', 'Berhasil Simpan');
        } else {
            return back()->with('status', 2)->with('message', 'Harap Isi Nominal KAS dan Sosial');
        }

    }

    public function edit(Request $request)
    {
        return Warga::find($request->id);
    }

    public function delete($id)
    {
        $data   =   Kas::find($id);
        $sosial =   Sosial::where('warga', $data->warga)->where('bulan', $data->bulan)->where('tanggal', $data->tanggal)->first();
        $sosial->delete();
        $data->delete();

        return back()->with('status', 1)->with('message', 'Berhasil Hapus');
    }
}
