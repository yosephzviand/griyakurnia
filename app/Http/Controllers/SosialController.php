<?php

namespace App\Http\Controllers;

use App\Models\Sosial;
use App\Models\Warga;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SosialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data   =   Sosial::get();
        $warga  =   Warga::whereNotIn('id', Sosial::select('warga')->where('bulan', date('m')))->get();
        return view('sosial', compact('data', 'warga'));
    }

    public function store(Request $request)
    {
        if ($request->idedit) {
            $data   =   Sosial::find($request->idedit);
        } else {
            $data   = new Sosial();
        }

        $data->warga    =   $request->warga;
        $data->bulan    =   $request->bulan;
        $data->nominal  =   15000;
        $data->tanggal  =   Carbon::now();
        $data->save();

        return back()->with('status', 1)->with('message', 'Berhasil Simpan');
    }

    public function delete($id)
    {
        $data   =   Sosial::find($id);
        $data->delete();

        return back()->with('status', 1)->with('message', 'Berhasil Hapus');
    }
}
