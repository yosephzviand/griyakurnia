<?php

namespace App\Http\Controllers;

use App\Models\Keluar;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KeluarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data   =   Keluar::get();
        return view('keluar', compact('data'));
    }

    public function store(Request $request)
    {
        if ($request->idedit) {
            $data   =   Keluar::find($request->idedit);
        } else {
            $data   =   new Keluar();
        }

        if ($request->jenis == 'kas') {
            $data->jenis = 1;
        } else {
            $data->jenis = 2;
        }
        $data->bulan        =   $request->bulan;
        $data->deskripsi    =   $request->deskripsi;
        $data->nominal      =   $request->nominal;
        $data->tanggal      =   Carbon::now();
        $data->save();

        return back()->with('status', 1)->with('message', 'Berhasil Simpan');
    }

    public function edit(Request $request)
    {
        return Keluar::find($request->id);
    }

    public function delete($id)
    {
        $data   =   Keluar::find($id);
        $data->delete();

        return back()->with('status', 1)->with('message', 'Berhasil Hapus');
    }
}
