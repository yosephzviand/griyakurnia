<?php

namespace App\Http\Controllers;

use App\Models\Blok;
use App\Models\Warga;
use Illuminate\Http\Request;

class WargaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data   =   Warga::get();
        $blok   =   Blok::get();
        return view('warga', compact('data', 'blok'));
    }

    public function store(Request $request)
    {
        if($request->idedit){
            $data   =   Warga::find($request->idedit);
        }else {
            $data   = new Warga();
        }

        $data->blok     =   $request->blok;
        $data->norumah  =   $request->norumah;
        $data->nama     =   $request->nama;
        $data->save();

        return back()->with('status', 1)->with('message', 'Berhasil Simpan');
    }

    public function edit(Request $request)
    {
        return Warga::find($request->id);
    }

    public function delete($id)
    {
        $data   =   Warga::find($id);
        $data->delete();

        return back()->with('status', 1)->with('message', 'Berhasil Hapus');
    }
}
