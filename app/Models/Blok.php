<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blok extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'blok';
}
