<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sosial extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'sosial';

    public function towarga()
    {
        return $this->belongsTo(Warga::class, 'warga', 'id');
    }
}
